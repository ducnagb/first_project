#include "cpp-visualizer.h"

using namespace std;


var_watcher<true> watcher;


struct ListNode {
    int value;
    ListNode* next;
};





int main() {
    int a = 10;
    int b = 20;
    int* p1 = &a;
    int* p2 = &b;

    watcher.watch(a, "a");
    watcher.watch(b, "b");
    watcher.watch(p1, "p1");
    watcher.watch(p2, "p2");


    ListNode n1 = { 10, nullptr };
    ListNode n2 = { 20, &n1 };
    ListNode n3 = { 30, &n2 };

    function<string(const ListNode&)> node_accessor = [](const ListNode& p) {
        ostringstream ss;
        ss << "(" << p.value << ", 0x" << hex << p.next << ")";
        return ss.str();
    };

    watcher.watch(n1, "n1", node_accessor);
    watcher.watch(n1.next, "n1.next", &n1);

    watcher.watch(n2, "n2", node_accessor);
    watcher.watch(n2.next, "n2.next", &n2);

    watcher.watch(n3, "n3", node_accessor);
    watcher.watch(n3.next, "n3.next", &n3);

    watcher.scan();
    watcher.dump();

    // make some changes
    b = 100;
    p2 = &a;

    n3.next = &n1;

    cout << endl;
    watcher.scan();
    watcher.dump();
    
    watcher.send();
}
