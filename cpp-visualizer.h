#include <list>
#include <string>
#include <memory>
#include <functional>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <typeinfo>

#include <nlohmann/json.hpp>
#include "Connect.h"

template <bool Enabled = true>
class var_watcher {
protected:
    struct var_info {
        int id;
        std::string name;
        const std::type_info& type;
        bool is_pointer;
        const void* pointer;
        const void* parent;
        std::function<std::string (const void*)> accessor;

        std::string value;
        bool changed = false;

        var_info(int _id, const char* _name, const std::type_info& _type, bool _is_pointer, const void* _pointer, std::function<std::string (const void*)> _accessor, const void* _parent)
            : id(_id),
            name(_name),
            type(_type),
            is_pointer(_is_pointer),
            pointer(_pointer),
            accessor(_accessor),
            parent(_parent)
        { }

        bool update() {
            std::string new_value;
            
            if (accessor != nullptr) {
                new_value = accessor(pointer);

            } else if (is_pointer) {
                std::ostringstream ss;
                ss << "0x" << std::hex << *(int*)pointer;
                new_value = ss.str();

            } else if (type == typeid(int)) {
                new_value = std::to_string(*(const int*)pointer);

            } else if (type == typeid(char)) {
                new_value = std::to_string((int)*(const char*)pointer);

            } else throw std::invalid_argument(name);

            if (new_value != value) {
                value = std::move(new_value);
                changed = true;
            }

            return changed;
        }
    };

    std::list< std::shared_ptr<var_info> > variables;


    int current_id = 1;

    int make_id() {
        return current_id++;
    }

    std::shared_ptr<var_info> find_variable_by_address(const void* pointer) const {
        for (auto& var : variables) {
            if (var->pointer == pointer) return var;
        }

        return nullptr;
    }


public:
    template <typename T> requires(!Enabled)
    void watch(const T& var, const char* name, const void* parent = nullptr)
    {}

    template <typename T> requires(!Enabled)
    void watch(const T& var, const char* name, std::function<std::string(const T&)> accessor, const void* parent = nullptr)
    {}

    // adds a non-pointer variable
    template <typename T> requires(Enabled && !std::is_pointer_v<T>)
    void watch(const T& var, const char* name, const void* parent = nullptr) {
        variables.push_back(std::make_shared<var_info>(make_id(), name, typeid(var), false, (const void*)&var, nullptr, parent));
    }

    // adds a pointer variable
    template <typename T> requires(Enabled && std::is_pointer_v<T>)
    void watch(const T& var, const char* name, const void* parent = nullptr) {
        variables.push_back(std::make_shared<var_info>(make_id(), name, typeid(var), true, (const void*)&var, nullptr, parent));
    }

    // adds a custom-typed variable
    template <typename T> requires(Enabled)
    void watch(const T& var, const char* name, std::function<std::string(const T&)> accessor, const void* parent = nullptr) {
        auto accessor_wrapper = [accessor](const void* p) {
            return accessor(*(const T*)p);
        };

        variables.push_back(std::make_shared<var_info>(make_id(), name, typeid(var), false, (const void*)&var, accessor_wrapper, parent));
    }



    // returns number of updated variables
    int scan() requires(!Enabled) {
        return 0;
    }

    int scan() requires(Enabled) {
        int counter = 0;

        for (auto& var : variables) {
            var->changed = false;
            if (var->update()) counter++;
        }

        return counter;
    }




    void dump(bool only_changed = true) const requires(!Enabled)
    {}

    void dump(bool only_changed = true) const requires(Enabled) {
        for (auto& var : variables) {
            if (only_changed && !var->changed) continue;

            std::cout << "id: " << var->id << ", name: " << var->name << ", type: " << var->type.name() << ", value: " << var->value;

            if (var->parent != nullptr) {
                auto parent_var = find_variable_by_address(var->parent);
                if (parent_var) std::cout << ", parent: " << parent_var->id;
            }

            std::cout << std::endl;
        }
    }




    // send JSON data to server
    void send(bool only_changed = true) const requires(Enabled) {
        nlohmann::json jsonData;

        for (auto& var : variables) {
            if (only_changed && !var->changed) continue;
            
            nlohmann::json varData;
            varData["id"] = var->id;
            varData["name"] = var->name;
            varData["type"] = var->type.name();
            varData["value"] = var->value;

            if (var->parent != nullptr) {
                auto parent_var = find_variable_by_address(var->parent);
                if (parent_var) varData["parent"] = parent_var->id;
            }

            jsonData.push_back(varData);
        }

        // Convert JSON object to string
        std::string jsonString = jsonData.dump();

        // Print JSON string
        std::cout << jsonString << std::endl;
        websocket_endpoint endpoint;
        
        std::string uri = "ws://localhost:9002"; // setting URI
        int connectionId = endpoint.connect(uri);
        if (connectionId != -1) {
        //std::cout << "> Created connection with id " << connectionId << std::endl;
        }

        Sleep(500);

        // Send a message to the server using the connection ID
        endpoint.send(connectionId, jsonString);
     }    
};
